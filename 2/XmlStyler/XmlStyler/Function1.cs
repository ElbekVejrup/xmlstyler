using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using OpenHtmlToPdf;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace XmlStyler
{
    public static class Function1
    {
        [FunctionName("XmlStyler")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req, ILogger log, ExecutionContext context)
        {
            try
            {
                var stylesheet = req.Query["stylesheet"];
                var output = req.Query["output"];

                var path = Path.GetFullPath(Path.Combine(context.FunctionDirectory, "..\\Styles\\" + stylesheet));
                if (string.IsNullOrEmpty(stylesheet)) { return new BadRequestObjectResult("Please pass a stylesheet on the query string."); }
                var body = await new StreamReader(req.Body).ReadToEndAsync();
                if (string.IsNullOrEmpty(body)) { return new BadRequestObjectResult("Please pass xml as base64 in the request body."); }
                body = body.Replace("77u/", "");
                var xmlString = DecodeStringFromBase64(body);
                var xmlDoc = CreateXmlDocument(xmlString);
                var result = ConvertXml(xmlDoc, path, new XsltArgumentList()).Replace("OIOUBL.css", stylesheet + "OIOUBL.css");

                if (output == "pdf")
                {
                    var pdf = BuildPdf(result);
                    return BuildResponse(pdf);
                }
                else if (output == "xml")
                {
                    var xml = PrintXML(xmlString);
                    return new FileContentResult(Encoding.ASCII.GetBytes(xml.ToString()), "text/html");
                }
                else
                {
                    return new OkObjectResult(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private static string DecodeStringFromBase64(string stringToDecode)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(stringToDecode));
        }

        private static XmlDocument CreateXmlDocument(string xmlString)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            return xmlDoc;
        }

        private static string ConvertXml(XmlDocument inputXmlDocument, string xsltFilePath, XsltArgumentList xsltArgs)
        {
            try
            {
                AppContext.SetSwitch("Switch.System.Xml.AllowDefaultResolver", true);
                var result = string.Empty;

                using (var stringWriter = new StringWriter())
                {
                    using (var stringReader = new StringReader(inputXmlDocument.OuterXml))
                    {
                        using (var xmlTextReader = new XmlTextReader(stringReader))
                        {
                            var xsltSettings = new XsltSettings(true, true);
                            var xmlUrlResolver = new XmlUrlResolver();

                            var xslCompiledTransform = new XslCompiledTransform(true);
                            xslCompiledTransform.Load(xsltFilePath, xsltSettings, xmlUrlResolver);
                            xslCompiledTransform.Transform(xmlTextReader, xsltArgs, stringWriter);
                            result = stringWriter.ToString();
                            xslCompiledTransform = null;
                        }
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private static byte[] BuildPdf(string html)
        {
            try
            {
                var result = Pdf
                   .From(html)
                   .Content();

                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private static FileContentResult BuildResponse(byte[] pdfBytes)
        {
            return new FileContentResult(pdfBytes, "application/pdf");
        }

        public static string PrintXML(string xml)
        {
            string result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(xml);

                writer.Formatting = System.Xml.Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                string formattedXml = sReader.ReadToEnd();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("<!DOCTYPE html>");
                stringBuilder.Append("<html>");
                stringBuilder.Append("<head>");
                stringBuilder.Append("<title>XML</title>");
                stringBuilder.Append("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/styles/default.min.css'>");
                stringBuilder.Append("<script src='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js'></script>");
                stringBuilder.Append("<script src='https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js'></script>");
                stringBuilder.Append("<style>.hljs{display:block;overflow-x:auto;padding:.5em;background:#fff;color:#000}.hljs-comment,.hljs-quote,.hljs-variable{color:green}.hljs-built_in,.hljs-keyword,.hljs-name,.hljs-selector-tag,.hljs-tag{color:#00f}.hljs-addition,.hljs-attribute,.hljs-literal,.hljs-section,.hljs-string,.hljs-template-tag,.hljs-template-variable,.hljs-title,.hljs-type{color:#a31515}.hljs-deletion,.hljs-meta,.hljs-selector-attr,.hljs-selector-pseudo{color:#2b91af}.hljs-doctag{color:grey}.hljs-attr{color:red}.hljs-bullet,.hljs-link,.hljs-symbol{color:#00b0e8}.hljs-emphasis{font-style:italic}.hljs-strong{font-weight:700}</style>");
                stringBuilder.Append("</head>");
                stringBuilder.Append("<body>");
                stringBuilder.Append("<pre><code class='xml'>");
                stringBuilder.Append(WebUtility.HtmlEncode(formattedXml));
                stringBuilder.Append("</code></pre>");
                // stringBuilder.Append("<script> (function () {var el = document.querySelector('.xml'), esc = _.escape(el.innerHTML); el.innerHTML = esc; hljs.highlightBlock(el); })();</script>");
                stringBuilder.Append("<script>hljs.initHighlightingOnLoad();</script>");
                stringBuilder.Append("</body>");
                stringBuilder.Append("</html>");

                result = stringBuilder.ToString();
            }
            catch (XmlException)
            {
                // Handle the exception
            }

            mStream.Close();
            writer.Close();

            return result;
        }
    }
}