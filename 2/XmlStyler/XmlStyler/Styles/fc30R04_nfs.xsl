﻿<?xml version="1.0" encoding="UTF-8"?>

<!--  ...................................................  -->
<!-- fc30R04.xsl                                           -->
<!-- 2006.04.18 (FC) Version 3.0 release 04                -->
<!-- (c)2006 EDImatic A/S                                  -->
<!--                                                       -->
<!-- Html konvertering af OIO faktura m. forbrugsafregning -->
<!--                 (PIE,PCM,PIP,PCP samt testvarianter)  -->
<!--                                                       -->
<!--  ...................................................  -->


<xsl:stylesheet version="1.0"
    xmlns:xsl= "http://www.w3.org/1999/XSL/Transform"
    xmlns:udk= "http://rep.oio.dk/ubl/xml/schemas/0p71/maindoc/"
    xmlns:com= "http://rep.oio.dk/ubl/xml/schemas/0p71/common/"
    xmlns:pie= "http://rep.oio.dk/ubl/xml/schemas/0p71/pie/"
    xmlns:tpcm="http://rep.oio.dk/ubl/xml/schemas/0p71/testpcm/"
    xmlns:tpcp="http://rep.oio.dk/ubl/xml/schemas/0p71/testpcp/"
    xmlns:tpie="http://rep.oio.dk/ubl/xml/schemas/0p71/testpie/"
    xmlns:tpip="http://rep.oio.dk/ubl/xml/schemas/0p71/testpip/"
    xmlns:pip= "http://rep.oio.dk/ubl/xml/schemas/0p71/pip/"
    xmlns:pcm= "http://rep.oio.dk/ubl/xml/schemas/0p71/pcm/"
    xmlns:pcp= "http://rep.oio.dk/ubl/xml/schemas/0p71/pcp/"
    xmlns:fsv= "http://rep.oio.dk/ubl/xml/schemas/0p71/extensible/businessdomain/utilities/2005/10/17/">

  <!--<xsl:output method="xml" indent="yes"/>-->
  <xsl:output method="html" />
  <xsl:template match="/">
    <xsl:apply-templates select="/*[local-name()='Invoice']"/>
  </xsl:template>

  <xsl:template match="udk:Invoice| pip:Invoice | pie:Invoice | pcm:Invoice |tpcm:Invoice |tpcp:Invoice|tpie:Invoice|tpip:Invoice | pcp:Invoice ">

    <xsl:variable name="fakturatype">
      <xsl:choose>
        <xsl:when test="contains(com:TypeCode, 'PIE')">FAKTURA</xsl:when>
        <xsl:when test="contains(com:TypeCode, 'PIP')">FAKTURA</xsl:when>
        <xsl:when test="contains(com:TypeCode, 'PCM')">KREDITNOTA</xsl:when>
        <xsl:when test="contains(com:TypeCode, 'PCP')">KREDITNOTA</xsl:when>
        <xsl:otherwise>Ukendt dokumenttype</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <!-- opretter HTML med max 4 tabeller -->
    <html>
      <head>
        <title>
          OIOXML fakturaudskrivning version 3.0 release 0.1 (<xsl:value-of select="com:TypeCode"/>)
        </title>
      </head>
      <body>
        <!-- Start på fakturahovedet -->
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td valign="top">
              <font face="Arial">
                <xsl:value-of select="$fakturatype"/>
              </font>
            </td>
            <td valign="top"></td>
            <td valign="top"></td>
            <td valign="top"></td>
          </tr>
          <tr>
            <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="2">
              <hr color="#77B321" size="5" NOSHADE="true"/>
            </td>
          </tr>

          <tr>
            <td valign="top" colspan="2" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <!-- indsætter køberadressen -->
                <b>Køber</b><br/>
                EAN: <xsl:value-of select="com:BuyersReferenceID"/><br/>

                <xsl:for-each select="com:BuyerParty/com:BuyerContact">
                  <xsl:choose>
                    <xsl:when test = "position() = '1'">
                      Ordrekontakt.: <xsl:value-of select="com:ID"/> (ID)<br/>
                      <xsl:if test = "com:Name != ''">
                        Ordrekontakt.: <xsl:value-of select="com:Name"/>  (Navn)<br/>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test = "position() = '2' and com:Role != ''">
                      <xsl:if test = "com:ID != ''">
                        Ordrekontakt.: <xsl:value-of select="com:ID"/> (<xsl:value-of select="com:Role"/>)<br/>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test = "position() = '3' and com:Role != ''">
                      <xsl:if test = "com:ID != ''">
                        Ordrekontakt.: <xsl:value-of select="com:ID"/> (<xsl:value-of select="com:Role"/>)<br/>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test = "position() = '4' and com:Role != ''">
                      <xsl:if test = "com:ID != ''">
                        Ordrekontakt.: <xsl:value-of select="com:ID"/> (<xsl:value-of select="com:Role"/>)<br/>
                      </xsl:if>
                    </xsl:when>
                  </xsl:choose>
                </xsl:for-each>

                DimensionsKonto: <xsl:value-of select="com:BuyerParty/com:AccountCode"/><br/>

              </font>
            </td>
            <td  valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <!-- indsætter juridisk adresse -->
                <b>Juridisk</b>
                <br/>
                <xsl:for-each select="com:BuyerParty">
                  <xsl:if test = "com:Address/com:ID = 'Juridisk'">
                    <xsl:if test = "com:PartyName != ''">
                      <xsl:value-of select="com:PartyName"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "com:Address/com:Street != ''">
                      <xsl:value-of select="com:Address/com:Street"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="com:Address/com:HouseNumber"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "com:Address/com:PostalZone != ''">
                      <xsl:value-of select="com:Address/com:PostalZone"/>
                      <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:value-of select="com:Address/com:CityName"/>
                    <br/>
                    <xsl:if test = "com:ID != ''">
                      ID.: <xsl:value-of select="com:ID"/><br/>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </font>
            </td>
            <td  valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <!-- indsætter Faktureringsadressen -->
                <b>Faktureringsadresse</b>
                <br/>
                <xsl:for-each select="com:BuyerParty">
                  <xsl:choose>
                    <xsl:when test = "com:Address/com:ID = 'Juridisk'">
                    </xsl:when>
                    <xsl:when test = "com:Address/com:ID = 'Fakturering'">
                      <xsl:if test = "com:PartyName != ''">
                        <xsl:value-of select="com:PartyName"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test = "com:Address/com:Street != ''">
                        <xsl:value-of select="com:Address/com:Street"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="com:Address/com:HouseNumber"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test = "com:Address/com:PostalZone != ''">
                        <xsl:value-of select="com:Address/com:PostalZone"/>
                        <xsl:text> </xsl:text>
                      </xsl:if>
                      <xsl:value-of select="com:Address/com:CityName"/>
                      <br/>
                      <xsl:if test = "com:ID != ''">
                        ID.: <xsl:value-of select="com:ID"/><br/>
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:if test = "com:PartyName != ''">
                        <xsl:value-of select="com:PartyName"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test = "com:Address/com:Street != ''">
                        <xsl:value-of select="com:Address/com:Street"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="com:Address/com:HouseNumber"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test = "com:Address/com:PostalZone != ''">
                        <xsl:value-of select="com:Address/com:PostalZone"/>
                        <xsl:text> </xsl:text>
                      </xsl:if>
                      <xsl:value-of select="com:Address/com:CityName"/>
                      <br/>
                      <xsl:if test = "com:ID != ''">
                        ID.: <xsl:value-of select="com:ID"/><br/>
                      </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </font>
            </td>
          </tr>

          <xsl:if test = "com:DestinationParty != ''">
            <tr>
              <td valign="top" colspan="2" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- bruges ej -->
                </font>
              </td>
              <td  valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- bruges ej -->
                </font>
              </td>
              <td  valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- indsætter Leveringsadressen -->
                  <b>Leveringsadresse</b>
                  <br/>
                  <xsl:for-each select="com:DestinationParty">
                    <xsl:if test = "com:Contact/com:Name != ''">
                      <xsl:value-of select="com:Contact/com:Name"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "com:PartyName != ''">
                      <xsl:value-of select="com:PartyName"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "com:Address/com:Street != ''">
                      <xsl:value-of select="com:Address/com:Street"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="com:Address/com:HouseNumber"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "com:Address/com:PostalZone != ''">
                      <xsl:value-of select="com:Address/com:PostalZone"/>
                      <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:value-of select="com:Address/com:CityName"/>
                    <br/>
                    <xsl:if test = "com:ID != ''">
                      ID.: <xsl:value-of select="com:ID"/><br/>
                    </xsl:if>
                  </xsl:for-each>
                </font>
              </td>
            </tr>
          </xsl:if>

          <tr>
            <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="1">
              <hr color="#77B321" size="2" NOSHADE="true"/>
            </td>
          </tr>


          <tr>
            <td valign="top" colspan="2" bgcolor="#FFFFFF">
              <font  face="Arial" size="2">
                <!-- indsætter leverandøradressen -->
                <b>Leverandør</b><br/>
                <xsl:value-of select="com:SellerParty/com:PartyName"/><br/>
                <xsl:value-of select="com:SellerParty/com:Address/com:Street"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="com:SellerParty/com:Address/com:HouseNumber"/><br/>
                <xsl:value-of select="com:SellerParty/com:Address/com:PostalZone"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="com:SellerParty/com:Address/com:CityName"/><br/>
                CVR.: <xsl:value-of select="com:SellerParty/com:PartyTaxScheme/com:CompanyTaxID"/><br/>
                <xsl:if test = "com:SellerParty/com:ID != ''">
                  ID.: <xsl:value-of select="com:SellerParty/com:ID"/><br/>
                </xsl:if>
              </font>
            </td>
            <td  valign="top" colspan="2" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <!-- indsætter kontaktoplysninger -->
                <b>Kontaktoplysninger</b><br/>
                <xsl:value-of select="com:SellerParty/com:OrderContact/com:Name"/><br/>
                Tlf.: <xsl:value-of select="com:SellerParty/com:OrderContact/com:Phone"/><br/>
                Fax.: <xsl:value-of select="com:SellerParty/com:OrderContact/com:Fax"/><br/>
                Email.: <xsl:value-of select="com:SellerParty/com:OrderContact/com:E-Mail"/><br/>
              </font>
            </td>
          </tr>

          <tr>
            <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="1">
              <hr color="#77B321" size="2" NOSHADE="true"/>
            </td>
          </tr>
          <tr>
            <td width="26%" valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <b>Fakturanr: </b>
                <!-- indsætter Fakturanummer -->
                <xsl:value-of select="com:ID"/>
              </font>
            </td>
            <td width="26%" valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <b>Købers ordrenr: </b>
                <!-- indsætter Ordrenr  -->
                <xsl:value-of select="com:ReferencedOrder/com:BuyersOrderID"/>
              </font>
            </td>
            <td width="23%" valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <b>Sælgers ordrenr: </b>
                <!-- indsætter Ordrenr  -->
                <xsl:value-of select="com:ReferencedOrder/com:SellersOrderID"/>
              </font>
            </td>
            <td width="27%" valign="top" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <b>Dato: </b>
                <!-- indsætter faktura dato -->
                <xsl:value-of select="com:IssueDate"/>
              </font>
            </td>
          </tr>
          <tr>
            <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="1">
              <hr color="#77B321" size="2" NOSHADE="true"/>
            </td>
          </tr>
        </table>
        <br/>
        <!-- Slut på fakturahovedet -->

        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td valign="top">
              <font face="Arial" size="2">
                <b>Varenr</b>
              </font>
            </td>
            <td valign="top">
              <font face="Arial" size="2">
                <b>Beskrivelse</b>
              </font>
            </td>
            <td valign="top">
              <font face="Arial" size="2">
                <b>Antal</b>
              </font>
            </td>
            <td valign="top">
              <font face="Arial" size="2">
                <b>Enhed</b>
              </font>
            </td>
            <td valign="top">
              <font face="Arial" size="2">
                <b>Enhedspris</b>
              </font>
            </td>
            <td valign="top">
              <font face="Arial" size="2">
                <b></b>
              </font>
            </td>
            <td valign="top" align="right">
              <font face="Arial" size="2">
                <b>Pris</b>
              </font>
              <br/>
            </td>
          </tr>

          <!-- indsætter Ordreliner -->
          <xsl:apply-templates select="com:InvoiceLine"/>
          <tr>
            <td colspan="7" valign="top" height="10">
              <font face="Arial" size="1"></font>
            </td>
          </tr>

          <tr>
            <td width="100%" valign="top" colspan="7" bgcolor="#FFFFFF" height="1">
              <hr color="#77B321" size="2" NOSHADE="true"/>
            </td>
          </tr>

          <tr>
            <td valign="top" bgcolor="#FFFFFF" colspan="6">
              <font face="Arial" size="2">
                <b>Liniesum i alt excl moms</b>
              </font>
            </td>
            <td valign="top" bgcolor="#FFFFFF" align="right">
              <font face="Arial" size="2">
                <!-- Totalpris eksklusiv moms -->
                <xsl:variable name="tot1" select="com:LegalTotals/com:LineExtensionTotalAmount"/>
                <xsl:value-of select="format-number($tot1, '##0.00')"/>
              </font>
            </td>
          </tr>
          <xsl:for-each select="com:AllowanceCharge">

            <xsl:variable name="ainfo">
              <xsl:choose>
                <xsl:when test="com:MultiplierReasonCode != ''">
                  <xsl:value-of select="com:MultiplierReasonCode"/>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:variable name="binfo">
              <xsl:choose>
                <xsl:when test="com:ChargeIndicator = 'false'">ej momspligtigt</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:variable name="cinfo">
              <xsl:choose>
                <xsl:when test="$ainfo != '' and $binfo != ''">
                  (<xsl:value-of select="$ainfo"/>, <xsl:value-of select="$binfo"/>)
                </xsl:when>
                <xsl:when test="$ainfo != '' and $binfo = ''">
                  (<xsl:value-of select="$ainfo"/>)
                </xsl:when>
                <xsl:when test="$ainfo = '' and $binfo != ''">
                  (<xsl:value-of select="$binfo"/>)
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <tr>
              <td valign="top" bgcolor="#FFFFFF" colspan="6">
                <font face="Arial" size="2">
                  <b>
                    <xsl:value-of select="com:ID"/>
                    <xsl:value-of select="$cinfo"/>
                  </b>
                </font>
              </td>
              <td valign="top" bgcolor="#FFFFFF" align="right">
                <font face="Arial" size="2">
                  <xsl:variable name="tot2" select="com:AllowanceChargeAmount"/>
                  <xsl:value-of select="format-number($tot2, '##0.00')"/>
                </font>
              </td>
            </tr>
          </xsl:for-each>

          <!-- Foerste gennemloeb -->
          <xsl:for-each select="com:TaxTotal">
            <xsl:variable name="nuller">
              <xsl:choose>
                <xsl:when test="com:TaxAmounts/com:TaxableAmount = ''">nul</xsl:when>
                <xsl:when test="com:TaxAmounts/com:TaxableAmount = '0'">nul</xsl:when>
                <xsl:when test="com:TaxAmounts/com:TaxableAmount = '0.0'">nul</xsl:when>
                <xsl:when test="com:TaxAmounts/com:TaxableAmount = '0.00'">nul</xsl:when>
                <xsl:when test="com:TaxAmounts/com:TaxableAmount = '0.000'">nul</xsl:when>
                <xsl:otherwise>ejnul</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:choose>
              <!-- Momsfri andel -->
              <xsl:when test = "com:TaxTypeCode = 'ZERO-RATED' and $nuller = 'ejnul'">
                <tr>
                  <td valign="top" bgcolor="#FFFFFF" colspan="6">
                    <font face="Arial" size="2">
                      <b>Momsfri andel</b>
                    </font>
                  </td>
                  <td valign="top" bgcolor="#FFFFFF" align="right">
                    <font face="Arial" size="2">
                      <!-- Momsfri andel -->
                      <xsl:variable name="tot3" select="com:TaxAmounts/com:TaxableAmount"/>
                      <xsl:value-of select="format-number($tot3, '##0.00')"/>
                    </font>
                  </td>
                </tr>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>

          <!-- Andet gennemloeb -->
          <xsl:for-each select="com:TaxTotal">
            <xsl:choose>
              <!-- Momspligtig andel -->
              <xsl:when test = "com:TaxTypeCode = 'VAT'">
                <tr>
                  <td valign="top" bgcolor="#FFFFFF" colspan="6">
                    <font face="Arial" size="2">
                      <b>Momsgrundlag</b>
                    </font>
                  </td>
                  <td valign="top" bgcolor="#FFFFFF" align="right">
                    <font face="Arial" size="2">
                      <!-- Momsgrundlag -->
                      <xsl:variable name="tot6" select="com:TaxAmounts/com:TaxableAmount"/>
                      <xsl:value-of select="format-number($tot6, '##0.00')"/>
                    </font>
                  </td>
                </tr>
                <tr>
                  <xsl:variable name="momspct" select="com:CategoryTotal/com:RatePercentNumeric"/>
                  <td valign="top" bgcolor="#FFFFFF" colspan="6">
                    <font face="Arial" size="2">
                      <b>
                        Total momsbeløb (<xsl:value-of select="format-number($momspct, '##0.00')"/>%)
                      </b>
                    </font>
                  </td>
                  <td valign="top" bgcolor="#FFFFFF" align="right">
                    <font face="Arial" size="2">
                      <!-- Totalmoms -->
                      <xsl:variable name="tot4" select="com:TaxAmounts/com:TaxAmount"/>
                      <xsl:value-of select="format-number($tot4, '##0.00')"/>
                    </font>
                  </td>
                </tr>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>

          <tr>
            <td valign="top" bgcolor="#FFFFFF" colspan="6">
              <font face="Arial" size="2">
                <b>Fakturatotal incl moms</b>
              </font>
            </td>
            <td valign="top" bgcolor="#FFFFFF" align="right">
              <font face="Arial" size="2">
                <!-- Total inklusiv moms -->
                <xsl:variable name="tot5" select="com:LegalTotals/com:ToBePaidTotalAmount"/>
                <xsl:value-of select="format-number($tot5, '##0.00')"/>
              </font>
            </td>
          </tr>
          <tr>
            <td width="100%" valign="top" colspan="7" bgcolor="#FFFFFF" height="2">
              <hr color="#77B321" size="5" NOSHADE="true"/>
            </td>
          </tr>

        </table>
        <br/>
        <!-- Slut på liniesektion -->

        <!-- Start på betalingsoplysninger -->
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td valign="top"></td>
            <td valign="top"></td>
            <td valign="top"></td>
            <td valign="top"></td>
          </tr>
          <tr>
            <td valign="top" colspan="4" bgcolor="#FFFFFF">
              <font face="Arial" size="2">
                <!-- her indsættes betalingsoplysninger -->
                <b>Betalingsoplysninger</b>
                <br/>
                <xsl:choose>
                  <!-- KONTOOVERFØRSEL -->
                  <xsl:when test = "com:PaymentMeans/com:PaymentChannelCode = 'KONTOOVERFØRSEL'">
                    Forfaldsdato: <xsl:value-of select="com:PaymentMeans/com:PaymentDueDate"/><br/>
                    Valutakode: <xsl:value-of select="udk:InvoiceCurrencyCode"/><br/>
                    Betalingstype: <xsl:value-of select="com:PaymentMeans/com:PaymentChannelCode"/><br/>
                    Kontotype.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:TypeCode"/><br/>
                    Regnr: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:FiBranch/com:ID"/><br/>
                    Kontonr.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:ID"/><br/>
                    Pengeinstitut: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:FiBranch/com:FinancialInstitution/com:Name"/><br/>
                  </xsl:when>

                  <!-- INDBETALINGSKORT -->
                  <xsl:when test = "com:PaymentMeans/com:PaymentChannelCode = 'INDBETALINGSKORT'">
                    Forfaldsdato: <xsl:value-of select="com:PaymentMeans/com:PaymentDueDate"/><br/>
                    Valutakode: <xsl:value-of select="udk:InvoiceCurrencyCode"/><br/>
                    Betalingstype: <xsl:value-of select="com:PaymentMeans/com:PaymentChannelCode"/><br/>
                    Kortart: <xsl:value-of select="com:PaymentMeans/com:TypeCodeID"/><br/>
                    Kontonr.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:ID"/><br/>
                    Kontotype.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:TypeCode"/><br/>
                    Betalingsid: <xsl:value-of select="com:PaymentMeans/com:PaymentID"/><br/>
                    Kreditornr: <xsl:value-of select="com:PaymentMeans/com:JointPaymentID"/><br/>
                    Advis: <xsl:value-of select="com:PaymentMeans/com:PaymentAdvice/com:LongAdvice"/><br/>
                  </xsl:when>

                  <!-- DIRECT DEBET -->
                  <xsl:when test = "com:PaymentMeans/com:PaymentChannelCode = 'DIRECT DEBET'">
                    Forfaldsdato: <xsl:value-of select="com:PaymentMeans/com:PaymentDueDate"/><br/>
                    Valutakode: <xsl:value-of select="udk:InvoiceCurrencyCode"/><br/>
                    Betalingstype: <xsl:value-of select="com:PaymentMeans/com:PaymentChannelCode"/><br/>
                  </xsl:when>

                  <!-- NATIONAL CLEARING -->
                  <xsl:when test = "com:PaymentMeans/com:PaymentChannelCode = 'NATIONAL CLEARING'">
                    Forfaldsdato: <xsl:value-of select="com:PaymentMeans/com:PaymentDueDate"/><br/>
                    Valutakode: <xsl:value-of select="udk:InvoiceCurrencyCode"/><br/>
                    Betalingstype: <xsl:value-of select="com:PaymentMeans/com:PaymentChannelCode"/><br/>
                  </xsl:when>

                  <!-- Ukendt betalingstype -->
                  <xsl:otherwise>
                    Forfaldsdato: <xsl:value-of select="com:PaymentMeans/com:PaymentDueDate"/><br/>
                    Valutakode: <xsl:value-of select="udk:InvoiceCurrencyCode"/><br/>
                    Betalingstype: <xsl:value-of select="com:PaymentMeans/com:PaymentChannelCode"/><br/>
                    Kontotype.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:TypeCode"/><br/>
                    Regnr: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:FiBranch/com:ID"/><br/>
                    Kontonr.: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:ID"/><br/>
                    Pengeinstitut: <xsl:value-of select="com:PaymentMeans/com:PayeeFinancialAccount/com:FiBranch/com:FinancialInstitution/com:Name"/><br/>
                    Kortart: <xsl:value-of select="com:PaymentMeans/com:TypeCodeID"/><br/>
                    Betalingsid: <xsl:value-of select="com:PaymentMeans/com:PaymentID"/><br/>
                    Kreditornr: <xsl:value-of select="com:PaymentMeans/com:JointPaymentID"/><br/>
                  </xsl:otherwise>
                </xsl:choose>

              </font>

            </td>
          </tr>
        </table>
        <br/>
        <!-- Slut på betalingsoplysninger -->


        <!-- Start på betalingsbetingelser -->
        <xsl:if test="com:PaymentTerms[.!='']">
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
              <td valign="top"></td>
              <td valign="top"></td>
              <td valign="top"></td>
              <td valign="top"></td>
            </tr>
            <tr>
              <td valign="top" colspan="4" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- her indsættes betalingsbetingelser -->
                  <b>Betalingsbetingelser</b><br/>
                  Type: <xsl:value-of select="com:PaymentTerms/com:ID"/><br/>
                  Kontantrabatdato: <xsl:value-of select="com:PaymentTerms/com:SettlementPeriod/com:EndDateTimeDate"/><br/>
                  Kontantrabatsats (promille): <xsl:value-of select="com:PaymentTerms/com:SettlementDiscountRateNumeric"/><br/>
                  Strafrentedato: <xsl:value-of select="substring(com:PaymentTerms/com:PenaltyPeriod/com:StartDateTime, 1, 10)"/><br/>
                  Strafrentesats (promille): <xsl:value-of select="com:PaymentTerms/com:PenaltySurchargeRateNumeric"/><br/>
                </font>
              </td>
            </tr>
          </table>
          <br/>
        </xsl:if>
        <!-- Slut på betalingsbetingelser -->


        <!-- Start på fritekst -->
        <xsl:if test="com:Note[.!='']">
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
              <td valign="top"></td>
              <td valign="top"></td>
              <td valign="top"></td>
              <td valign="top"></td>
            </tr>
            <tr>
              <td valign="top" colspan="4" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- her indsættes fritekst -->
                  <b>Yderligere oplysninger</b>
                  <pre>
                    <font size="2">
                      <xsl:value-of select="com:Note"/>
                    </font>
                  </pre>
                  <br/>
                </font>
              </td>
            </tr>
          </table>
        </xsl:if>
        <!-- Slut på fritekst -->
        <!-- Start på FSV Extensible content -->
        <xsl:if test="com:ExtensibleContent/fsv:FSV[.!='']">
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
              <td width="100%" valign="top" colspan="7" bgcolor="#FFFFFF" height="2">
                <hr color="#77B321" size="5" NOSHADE="true"/>
              </td>
            </tr>
            <tr>
              <td valign="top" colspan="4" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- her indsættes FSV indhold -->
                  <b>Forbrugsafregning, detaljeret specifikation (FSV)</b>
                  <br/>
                </font>
              </td>
            </tr>
          </table>
          <xsl:apply-templates select="com:ExtensibleContent/fsv:FSV/fsv:UtilityInvoice"/>
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
              <br/>
            </tr>
            <tr>
              <td width="100%" valign="top" colspan="7" bgcolor="#FFFFFF" height="2">
                <hr color="#77B321" size="5" NOSHADE="true"/>
              </td>
            </tr>
          </table>
        </xsl:if>
        <!-- Slut på FSV Extensible content -->

      </body>
    </html>
  </xsl:template>

  <!-- Start på linie template -->
  <xsl:template match="com:InvoiceLine">
    <!-- indsætter 3 koloner for hver fakturalinie,en med materiale og pris oplysninger, en med supl. oplysninger og en med luft -->
    <tr>
      <td width="17%" valign="top">
        <font face="Arial" size="2">
          <!-- indsætter EAN varenr -->
          <xsl:value-of select="com:Item/com:ID"/>
        </font>
      </td>
      <td width="50%" valign="top">
        <font face="Arial" size="2" id="ItemDescription">
          <!-- indsætter varebeskrivelse -->
          <!--<xsl:value-of select ="com:Item/com:SellersItemIdentification/com:Description"/>-->
          <xsl:choose>
            <xsl:when test = "com:Item/com:SellersItemIdentification/com:Description != ''">
              <xsl:value-of select ="com:Item/com:SellersItemIdentification/com:Description"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="com:Item/com:Description"/>
            </xsl:otherwise>
          </xsl:choose>
        </font>
      </td>
      <td width="10%" valign="top">
        <font face="Arial" size="2">
          <!-- indsætter antal -->
          <xsl:variable name="antal" select="com:InvoicedQuantity"/>
          <xsl:value-of select="format-number($antal, '##0.00')"/>
        </font>
      </td>
      <td width="10%" valign="top">
        <font face="Arial" size="2">
          <!-- indsætter enhed -->
          <xsl:value-of select="com:InvoicedQuantity/@unitCode"/>
        </font>
      </td>
      <td width="5%" valign="top">
        <font face="Arial" size="2">
          <!-- indsætter enhedspris -->
          <xsl:variable name="enhedspris" select="com:BasePrice/com:PriceAmount"/>
          <xsl:value-of select="format-number($enhedspris, '##0.00')"/>
        </font>
      </td>
      <td width="3%" valign="top">
        <font face="Arial" size="2">
          <!-- indsætter tom felt -->
        </font>
      </td>
      <td width="13%" valign="top" align="right" style="white-space: nowrap">
        <font face="Arial" size="2">
          <!-- indsætter linietotal -->
          <xsl:variable name="linietotal" select="com:LineExtensionAmount"/>
          <xsl:value-of select="format-number($linietotal, '##0.00')"/>
        </font>
      </td>
    </tr>
    <tr>
      <td valign="top">
        <font face="Arial" size="2"></font>
      </td>
      <td colspan="1" valign="top">
        <font face="Arial" size="1">
          <!-- indsætter flere ordreoplysniger -->
          <xsl:if test="com:Item/com:AdditionalItemIdentification/com:ID[.!='']">
            <xsl:value-of select="com:Item/com:AdditionalItemIdentification/com:ID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Item/com:BuyersItemIdentification/com:ID[.!='']">
            <b>Købers varenr.: </b>
            <xsl:value-of select="com:Item/com:BuyersItemIdentification/com:ID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Item/com:SellersItemIdentification/com:ID[.!='']">
            <b>Leverandørens varenr.: </b>
            <xsl:value-of select="com:Item/com:SellersItemIdentification/com:ID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Item/com:StandardItemIdentification/com:ID[.!='']">
            <b>Standard varenr.: </b>
            <xsl:value-of select="com:Item/com:StandardItemIdentification/com:ID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Item/com:CommodityClassification/com:CommodityCode[.!='']">
            <b>UNSPSC code.: </b>
            <xsl:value-of select="com:Item/com:CommodityClassification/com:CommodityCode"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Note[.!='']">
            <b>Note.: </b>
            <xsl:value-of select="com:Note"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:Item/com:Tax/com:RateCategoryCodeID = 'ZERO-RATED'">
            <b>Afgiftskategori.: </b> ej momspligtig<br/>
          </xsl:if>
          <xsl:for-each select="com:AllowanceCharge">
            <xsl:variable name="ainfo">
              <xsl:choose>
                <xsl:when test="com:MultiplierReasonCode != ''">
                  <xsl:value-of select="com:MultiplierReasonCode"/>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="binfo">
              <xsl:choose>
                <xsl:when test="com:ChargeIndicator = 'false'">ej momspligtigt</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="cinfo">
              <xsl:choose>
                <xsl:when test="$ainfo != '' and $binfo != ''">
                  (<xsl:value-of select="$ainfo"/>, <xsl:value-of select="$binfo"/>)
                </xsl:when>
                <xsl:when test="$ainfo != '' and $binfo = ''">
                  (<xsl:value-of select="$ainfo"/>)
                </xsl:when>
                <xsl:when test="$ainfo = '' and $binfo != ''">
                  (<xsl:value-of select="$binfo"/>)
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="tot2" select="com:AllowanceChargeAmount"/>
            <b>
              <xsl:value-of select="com:ID"/>.:
            </b>
            <xsl:value-of select="$cinfo"/>
            <xsl:value-of select="format-number($tot2, '##0.00')"/>
            <br/>
          </xsl:for-each>
          <xsl:if test="com:ReferencedOrderLine/com:BuyersID[.!='']">
            <b>Købers ordrenr.: </b>
            <xsl:value-of select="com:ReferencedOrderLine/com:BuyersID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:ReferencedOrderLine/com:SellersID[.!='']">
            <b>Sælgers ordrenr.: </b>
            <xsl:value-of select="com:ReferencedOrderLine/com:SellersID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:ReferencedOrderLine/com:DestinationParty/com:Contact/com:ID[.!='']">
            <b>Ordrekontakt.: </b>
            <xsl:value-of select="com:ReferencedOrderLine/com:DestinationParty/com:Contact/com:ID"/>
            <br/>
          </xsl:if>
          <xsl:if test="com:ReferencedOrderLine/com:DestinationParty/com:AccountCode[.!='']">
            <b>DimensionsKonto.: </b>
            <xsl:value-of select="com:ReferencedOrderLine/com:DestinationParty/com:AccountCode"/>
            <br/>
          </xsl:if>
          <xsl:if test = "com:ReferencedOrderLine/com:DestinationParty != ''">
            <b>Leveringsadresse.:</b>
            <br/>
            <xsl:for-each select="com:ReferencedOrderLine/com:DestinationParty">
              <xsl:if test = "com:Contact/com:Name != ''">
                <xsl:value-of select="com:Contact/com:Name"/>
                <br/>
              </xsl:if>
              <xsl:if test = "com:PartyName != ''">
                <xsl:value-of select="com:PartyName"/>
                <br/>
              </xsl:if>
              <xsl:if test = "com:Address/com:Street != ''">
                <xsl:value-of select="com:Address/com:Street"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="com:Address/com:HouseNumber"/>
                <br/>
              </xsl:if>
              <xsl:if test = "com:Address/com:PostalZone != ''">
                <xsl:value-of select="com:Address/com:PostalZone"/>
                <xsl:text> </xsl:text>
              </xsl:if>
              <xsl:value-of select="com:Address/com:CityName"/>
              <br/>
              <xsl:if test = "com:ID != ''">
                ID.: <xsl:value-of select="com:ID"/><br/>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>

          <!-- Delivery requirement -->
          <xsl:if test = "com:ReferencedOrderLine/com:DeliveryRequirement != ''">
            <xsl:choose>
              <!-- Periode -->
              <xsl:when test = "com:ReferencedOrderLine/com:DeliveryRequirement/com:ID = 'Period'">
                <b>Leveringsperiode.: </b>
                <xsl:for-each select="com:ReferencedOrderLine/com:DeliveryRequirement/com:DeliverySchedule">
                  <xsl:if test = "com:ID = '3' or com:ID = '4'">
                    fra <xsl:value-of select="substring(com:RequestedDeliveryDateTime, 1, 10)"/>
                    <xsl:text> </xsl:text>
                  </xsl:if>
                </xsl:for-each>
                <xsl:for-each select="com:ReferencedOrderLine/com:DeliveryRequirement/com:DeliverySchedule">
                  <xsl:if test = "com:ID = '1' or com:ID = '2'">
                    til <xsl:value-of select="substring(com:RequestedDeliveryDateTime, 1, 10)"/>
                    <xsl:text> </xsl:text>
                  </xsl:if>
                </xsl:for-each>
                <br/>
              </xsl:when>
              <!-- Dato -->
              <xsl:when test = "com:ReferencedOrderLine/com:DeliveryRequirement/com:ID = 'Deliverydate'">
                <b>Leveringsdato.: </b>
                <xsl:for-each select="com:ReferencedOrderLine/com:DeliveryRequirement/com:DeliverySchedule">
                  <xsl:if test = "com:ID = '1' or com:ID = '2'">
                    <xsl:value-of select="substring(com:RequestedDeliveryDateTime, 1, 10)"/>
                    <xsl:text> </xsl:text>
                  </xsl:if>
                </xsl:for-each>
                <br/>
              </xsl:when>
            </xsl:choose>
          </xsl:if>

        </font>
      </td>
      <td valign="top">
        <font face="Arial"></font>
      </td>
      <td valign="top">
        <font face="Arial"></font>
      </td>
      <td valign="top">
        <font face="Arial"></font>
      </td>
    </tr>
    <tr>
      <td colspan="7" valign="top" height="8">
        <font face="Arial" size="1"></font>
      </td>
    </tr>
  </xsl:template>


  <!-- Start på FSV Utility Invoice template -->
  <xsl:template match="com:ExtensibleContent/fsv:FSV/fsv:UtilityInvoice">
    <!-- Start på Utility invoice (forbrugssted) -->
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="1">
          <hr color="#77B321" size="2" NOSHADE="true"/>
        </td>
      </tr>

      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <!-- indsætter fakturainformation labels -->
            <b>Fakturainformation</b><br/>
            Fakturanr<br/>
            Kategori<br/>
            <xsl:if test="fsv:DeliveryDate[.!='']">
              Leveringsdato<br/>
            </xsl:if>
            Periode<br/>
            Beskrivelse<br/>
            Købers reference<br/>
            Købers kontonr<br/>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <!-- indsætter fakturainformation værdier -->
            <br/>
            <xsl:value-of select="fsv:UtilityInvoiceID"/>
            <br/>
            <xsl:value-of select="fsv:UtilityInvoiceCategoryCode"/>
            <br/>
            <xsl:if test="fsv:DeliveryDate[.!='']">
              <xsl:value-of select="fsv:DeliveryDate"/>
              <br/>
            </xsl:if>
            <xsl:value-of select="fsv:UtilityInvoiceMainPeriod/fsv:StartDate"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="fsv:UtilityInvoiceMainPeriod/fsv:EndDate"/>
            <br/>
            <xsl:value-of select="fsv:Description"/>
            <br/>
            <xsl:value-of select="fsv:BuyerParty/fsv:RefID"/>
            <br/>
            <xsl:value-of select="fsv:BuyerParty/fsv:AccountCode"/>
            <br/>
          </font>
        </td>
      </tr>

      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <!-- indsætter forbrugssted data labels -->
            <b>Forbrugssted data</b><br/>
            Forbrugsstedsnr<br/>
            Målernr<br/>
            Beskrivelse<br/>
            <xsl:if test="fsv:ConsumptionPoint/fsv:AdditionalSpecification[.!='']">
              Yderligere specifikation:<br/>
              ...Hovednr<br/>
              ...Beskrivelse<br/>
            </xsl:if>
            Adresse:<br/>
            ...Navn<br/>
            ...Vej<br/>
            ...By<br/>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <!-- indsætter forbrugssted data værdier -->
            <br/>
            <xsl:value-of select="fsv:ConsumptionPoint/fsv:ConsumptionPointID"/>
            <br/>
            <xsl:value-of select="fsv:ConsumptionPoint/fsv:MeterNumber"/>
            <br/>
            <xsl:value-of select="fsv:ConsumptionPoint/fsv:Comment"/>
            <br/>
            <xsl:if test="fsv:ConsumptionPoint/fsv:AdditionalSpecification[.!='']">
              <br/>
              <xsl:value-of select="fsv:ConsumptionPoint/fsv:AdditionalSpecification/fsv:MainSubscriberID/fsv:SubscriberID"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="fsv:ConsumptionPoint/fsv:AdditionalSpecification/fsv:MainSubscriberID/fsv:Type"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="fsv:ConsumptionPoint/fsv:AdditionalSpecification/fsv:MainSubscriberID/fsv:Comment"/>
              <br/>
              <xsl:value-of select="fsv:ConsumptionPoint/fsv:AdditionalSpecification/fsv:AdditionalDescription"/>
              <br/>
            </xsl:if>
            <br/>
            <xsl:value-of select="fsv:ConsumptionPointAddress/fsv:Name"/>
            <br/>
            <xsl:value-of select="fsv:ConsumptionPointAddress/fsv:DeliveryAddress/fsv:Street"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="fsv:ConsumptionPointAddress/fsv:DeliveryAddress/fsv:HouseNumber"/>
            <br/>
            <xsl:value-of select="fsv:ConsumptionPointAddress/fsv:DeliveryAddress/fsv:PostalZone"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="fsv:ConsumptionPointAddress/fsv:DeliveryAddress/fsv:CityName"/>
            <br/>
          </font>
        </td>
      </tr>

      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <!-- indsætter måler(e) og måledata labels -->
            <b>Måler(e) og måledata</b><br/>
            <xsl:for-each select="fsv:DeliveryMeterInformation/fsv:MeterSpecification">
              <xsl:for-each select="fsv:MeteringPoint">
                Målernr<br/>
                Beregningskonstant<br/>
                Metode<br/>
                Beskrivelse<br/>
                <xsl:for-each select="fsv:DeliveredQuantityBasedOnMeterReadings/fsv:MeterReading">
                  Forrige aflæsningsdato<br/>
                  Forrige aflæsning<br/>
                  Seneste aflæsningsdato<br/>
                  Seneste aflæsning<br/>
                  Forbrug<br/>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:for-each>
            Samlet forbrug<br/>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <!-- indsætter måler(e) og måledata værdier -->
            <br/>
            <xsl:for-each select="fsv:DeliveryMeterInformation/fsv:MeterSpecification">
              <xsl:for-each select="fsv:MeteringPoint">
                <xsl:value-of select="fsv:MeterNumber"/>
                <br/>
                <xsl:value-of select="fsv:MeterConstant"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="fsv:MeterConstant/@quantityUnitCode"/>
                <br/>
                <xsl:value-of select="fsv:MeterReadingMethod"/>
                <br/>
                <xsl:value-of select="fsv:MeterReadingComments"/>
                <br/>
                <xsl:for-each select="fsv:DeliveredQuantityBasedOnMeterReadings/fsv:MeterReading">
                  <xsl:value-of select="fsv:PreviousMeterReadingDate"/>
                  <br/>
                  <xsl:value-of select="fsv:PreviousMeterStand"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="fsv:PreviousMeterStand/@quantityUnitCode"/>
                  <br/>
                  <xsl:value-of select="fsv:LatestMeterReadingDate"/>
                  <br/>
                  <xsl:value-of select="fsv:LatestMeterStand"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="fsv:LatestMeterStand/@quantityUnitCode"/>
                  <br/>
                  <xsl:value-of select="fsv:DeliveredQuantity"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="fsv:DeliveredQuantity/@quantityUnitCode"/>
                  <br/>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:value-of select="fsv:DeliveryMeterInformation/fsv:TotalDeliveredQuantity"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="fsv:DeliveryMeterInformation/fsv:TotalDeliveredQuantity/@quantityUnitCode"/>
            <br/>
          </font>
        </td>
      </tr>
    </table>
    <br/>


    <xsl:for-each select="fsv:UtilitySubInvoiceSpecification">
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr></tr>
        <tr>
          <td width="30%" valign="top" bgcolor="#FFFFFF">
            <font  face="Arial" size="2">
              <!-- indsætter leverandør information labels -->
              <b>Specifikation pr. leverandør</b><br/>
              Leverandør:<br/>
              ...ID<br/>
              ...Type1<br/>
              ...Type2<br/>
              ...Beskrivelse<br/>
              ...Navn<br/>
              <xsl:for-each select="fsv:UtilitySubInvoiceHeader/fsv:Descriptions/fsv:Description">
                Beskrivelse<br/>
              </xsl:for-each>
              <xsl:if test="fsv:UtilitySubInvoiceHeader/fsv:Customer[.!='']">
                Køber:<br/>
                ...ID<br/>
                ...Type1<br/>
                ...Type2<br/>
                ...Beskrivelse<br/>
                ...Navn<br/>
                Installations adresse:<br/>
                ...ID<br/>
                ...Vej<br/>
                ...By<br/>
              </xsl:if>
            </font>
          </td>
          <td  width="70%" valign="top" bgcolor="#FFFFFF">
            <font face="Arial" size="2">
              <!-- indsætter leverandør information data værdier -->
              <br/>
              <br/>
              <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Supplier/fsv:PartyIdentification/fsv:PartyID"/>
              <br/>
              <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Supplier/fsv:PartyIdentification/fsv:Type"/>
              <br/>
              <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Supplier/fsv:PartyIdentification/fsv:SubType"/>
              <br/>
              <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Supplier/fsv:PartyIdentification/fsv:Comment"/>
              <br/>
              <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Supplier/fsv:PartyName/fsv:Name"/>
              <br/>
              <xsl:for-each select="fsv:UtilitySubInvoiceHeader/fsv:Descriptions/fsv:Description">
                <xsl:value-of select="."/>
                <br/>
              </xsl:for-each>
              <xsl:if test="fsv:UtilitySubInvoiceHeader/fsv:Customer[.!='']">
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:PartyIdentification/fsv:PartyID"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:PartyIdentification/fsv:Type"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:PartyIdentification/fsv:SubType"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:PartyIdentification/fsv:Comment"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:PartyName/fsv:Name"/>
                <br/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:InstallationAddress/fsv:InstallationID"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:InstallationAddress/fsv:Street"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:InstallationAddress/fsv:HouseNumber"/>
                <br/>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:InstallationAddress/fsv:PostalZone"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="fsv:UtilitySubInvoiceHeader/fsv:Customer/fsv:InstallationAddress/fsv:CityName"/>
                <br/>
              </xsl:if>
            </font>
          </td>
        </tr>
        <tr></tr>
      </table>

      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td valign="top">
            <font face="Arial" size="2">
              <b>Id</b>
            </font>
          </td>
          <td valign="top">
            <font face="Arial" size="2">
              <b>Beskrivelse</b>
            </font>
          </td>
          <td valign="top">
            <font face="Arial" size="2">
              <b>Antal</b>
            </font>
          </td>
          <td valign="top">
            <font face="Arial" size="2">
              <b>Enhed</b>
            </font>
          </td>
          <td valign="top">
            <font face="Arial" size="2">
              <b>Enhedspris</b>
            </font>
          </td>
          <td valign="top">
            <font face="Arial" size="2"></font>
          </td>
          <td valign="top" align="right">
            <font face="Arial" size="2">
              <b>Pris</b>
            </font>
            <br/>
          </td>
        </tr>

        <xsl:for-each select="fsv:UtilitySubInvoiceDetails/fsv:UtilitySubInvoiceLine">
          <tr>
            <td width="17%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter LineID -->
                <xsl:value-of select="fsv:LineID"/>
              </font>
            </td>
            <td width="50%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter ItemDescription -->
                <xsl:value-of select="fsv:Item/fsv:Description"/>/
                <xsl:value-of select="fsv:Item/fsv:ItemDescription"/>
              </font>
            </td>
            <td width="10%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter antal -->
                <xsl:variable name="antal" select="fsv:InvoicedQuantity"/>
                <xsl:value-of select="format-number($antal, '##0.00')"/>
              </font>
            </td>
            <td width="10%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter enhed -->
                <xsl:value-of select="fsv:InvoicedQuantity/@quantityUnitCode"/>
              </font>
            </td>
            <td width="5%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter enhedspris -->
                <xsl:variable name="enhedspris" select="fsv:Item/fsv:BasePrice/fsv:PriceAmount"/>
                <xsl:value-of select="format-number($enhedspris, '##0.00')"/>
              </font>
            </td>
            <td width="3%" valign="top">
              <font face="Arial" size="2">
                <!-- indsætter tom felt -->
              </font>
            </td>
            <td width="13%" valign="top" align="right">
              <font face="Arial" size="2">
                <!-- indsætter linietotal -->
                <xsl:variable name="linietotal" select="fsv:LineExtensionAmount"/>
                <xsl:value-of select="format-number($linietotal, '##0.00')"/>
              </font>
            </td>
          </tr>

          <tr>
            <td valign="top">
              <font face="Arial" size="2"></font>
            </td>
            <td colspan="1" valign="top">
              <font face="Arial" size="1">
                <!-- indsætter flere linieoplysniger -->
                <xsl:if test="fsv:Item/fsv:ID[.!='']">
                  <b>ID.: </b>
                  <xsl:value-of select="fsv:Item/fsv:ID"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:SubscriberID[.!='']">
                  <b>Abonnementnr.: </b>
                  <xsl:value-of select="fsv:Item/fsv:SubscriberID"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:Comment[.!='']">
                  <b>Beskrivelse.: </b>
                  <xsl:value-of select="fsv:Item/fsv:Comment"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:ConsumptionType[.!='']">
                  <b>Forbrugstype.: </b>
                  <xsl:value-of select="fsv:Item/fsv:ConsumptionType"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:CurrentChargeType[.!='']">
                  <b>Opkrævningstype1.: </b>
                  <xsl:value-of select="fsv:Item/fsv:CurrentChargeType"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:OneTimeChargeType[.!='']">
                  <b>Opkrævningstype2.: </b>
                  <xsl:value-of select="fsv:Item/fsv:OneTimeChargeType"/>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:PackQuantity[.!='']">
                  <b>Enhed.: </b>
                  <xsl:value-of select="fsv:Item/fsv:PackQuantity"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="fsv:Item/fsv:PackQuantity/@quantityUnitCode"/>
                  <br/>
                </xsl:if>
                <xsl:if test = "fsv:Item/fsv:Discount != ''">
                  <b>Rabat.: </b>
                  <xsl:for-each select="fsv:Item/fsv:Discount">
                    <xsl:if test = "fsv:RatePercentNumeric != ''">
                      <xsl:value-of select="fsv:RatePercentNumeric"/>
                      <xsl:text>%, </xsl:text>
                    </xsl:if>
                    <xsl:if test = "fsv:PriceAmount != ''">
                      <xsl:value-of select="fsv:PriceAmount"/>
                      <xsl:text>kr, </xsl:text>
                    </xsl:if>
                    <xsl:if test = "fsv:Quantity != ''">
                      <xsl:value-of select="fsv:Quantity"/>
                      <xsl:text></xsl:text>
                      <xsl:value-of select="fsv:Quantity/@quantityUnitCode"/>
                      <br/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:TaxCategory[.!='']">
                  <b>Momspct.: </b>
                  <xsl:value-of select="fsv:Item/fsv:TaxCategory/fsv:RatePercentNumeric"/>
                  <xsl:text> %</xsl:text>
                  <br/>
                </xsl:if>
                <xsl:if test="fsv:Item/fsv:Contract[.!='']">
                  <b>Kontraktnr.: </b>
                  <xsl:value-of select="fsv:Item/fsv:Contract/fsv:ContractID"/>
                  <br/>
                </xsl:if>
                <xsl:if test = "fsv:Item/fsv:ItemAddress != ''">
                  <b>Adresse.:</b>
                  <br/>
                  <xsl:for-each select="fsv:Item/fsv:ItemAddress">
                    <xsl:if test = "fsv:Name != ''">
                      <xsl:value-of select="fsv:Name"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "fsv:Street != ''">
                      <xsl:value-of select="fsv:Street"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="fsv:HouseNumber"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "fsv:PostalZone != ''">
                      <xsl:value-of select="fsv:PostalZone"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="fsv:CityName"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test = "fsv:ID != ''">
                      ID.: <xsl:value-of select="fsv:ID"/><br/>
                    </xsl:if>
                    <xsl:if test = "fsv:ID != ''">
                      Beskrivelse.: <xsl:value-of select="fsv:Comment"/><br/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>

              </font>
            </td>
            <td valign="top">
              <font face="Arial"></font>
            </td>
            <td valign="top">
              <font face="Arial"></font>
            </td>
            <td valign="top">
              <font face="Arial"></font>
            </td>
          </tr>

        </xsl:for-each>

        <tr></tr>

        <!-- Foerste gennemloeb -->
        <xsl:for-each select="fsv:UtilitySubInvoiceTotals/fsv:UtilitySubTaxTotal">
          <xsl:variable name="nuller">
            <xsl:choose>
              <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = ''">nul</xsl:when>
              <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0'">nul</xsl:when>
              <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.0'">nul</xsl:when>
              <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.00'">nul</xsl:when>
              <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.000'">nul</xsl:when>
              <xsl:otherwise>ejnul</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:choose>
            <!-- Momsfri andel -->
            <xsl:when test = "fsv:TaxTypeCode = 'ZERO-RATED' and $nuller = 'ejnul'">
              <tr>
                <td valign="top" bgcolor="#FFFFFF" colspan="6">
                  <font face="Arial" size="2">Momsfri andel</font>
                </td>
                <td valign="top" bgcolor="#FFFFFF" align="right">
                  <font face="Arial" size="2">
                    <!-- Momsfri andel -->
                    <xsl:variable name="tot3" select="fsv:TaxAmounts/fsv:TaxableAmount"/>
                    <xsl:value-of select="format-number($tot3, '##0.00')"/>
                  </font>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>

        <!-- Andet gennemloeb -->
        <xsl:for-each select="fsv:UtilitySubInvoiceTotals/fsv:UtilitySubTaxTotal">
          <xsl:choose>
            <!-- Momspligtig andel -->
            <xsl:when test = "fsv:TaxTypeCode = 'VAT'">
              <tr>
                <td valign="top" bgcolor="#FFFFFF" colspan="6">
                  <font face="Arial" size="2">Momsgrundlag</font>
                </td>
                <td valign="top" bgcolor="#FFFFFF" align="right">
                  <font face="Arial" size="2">
                    <!-- Momsgrundlag -->
                    <xsl:variable name="tot6" select="fsv:TaxAmounts/fsv:TaxableAmount"/>
                    <xsl:value-of select="format-number($tot6, '##0.00')"/>
                  </font>
                </td>
              </tr>
              <tr>
                <xsl:variable name="momspct" select="fsv:CategoryTotal/fsv:RatePercentNumeric"/>
                <td valign="top" bgcolor="#FFFFFF" colspan="6">
                  <font face="Arial" size="2">
                    Total momsbeløb (<xsl:value-of select="format-number($momspct, '##0.00')"/>%)
                  </font>
                </td>
                <td valign="top" bgcolor="#FFFFFF" align="right">
                  <font face="Arial" size="2">
                    <!-- Totalmoms -->
                    <xsl:variable name="tot4" select="fsv:TaxAmounts/fsv:TaxAmount"/>
                    <xsl:value-of select="format-number($tot4, '##0.00')"/>
                  </font>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>

        <tr>
          <td valign="top" bgcolor="#FFFFFF" colspan="6">
            <font face="Arial" size="2">Leverandørtotal incl moms</font>
          </td>
          <td valign="top" bgcolor="#FFFFFF" align="right">
            <font face="Arial" size="2">
              <!-- Total inklusiv moms -->
              <xsl:variable name="tot5" select="fsv:UtilitySubInvoiceTotals/fsv:UtilitySubLegalTotals/fsv:ToBePaidTotalAmount"/>
              <xsl:value-of select="format-number($tot5, '##0.00')"/>
            </font>
          </td>
        </tr>
      </table>
      <br/>
    </xsl:for-each>
    <!-- loop pr Utility Sub Invoice -->


    <table border="0" width="100%" cellspacing="0" cellpadding="2">

      <!-- indsætter udvikling i forbrug -->
      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <b>Udvikling i forbrug</b>
            <br/>
            <xsl:for-each select="fsv:DevelopmentInConsumption">
              Forbrug<br/>
              Periode<br/>
            </xsl:for-each>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <br/>
            <xsl:for-each select="fsv:DevelopmentInConsumption">
              <xsl:value-of select="fsv:Quantity"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="fsv:Quantity/@quantityUnitCode"/>
              <br/>
              <xsl:value-of select="fsv:PeriodOfConsumption"/>
              <br/>
            </xsl:for-each>
          </font>
        </td>
      </tr>


      <!-- indsætter a conto plan -->
      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <b>A conto plan</b>
            <br/>
            <xsl:for-each select="fsv:DeliveryOnAccountPaymentSchedule">
              <xsl:for-each select="fsv:OnAccountPayments">
                <xsl:for-each select="fsv:DuePaymentDate">
                  Betalingsdato<br/>
                </xsl:for-each>
                Beløb<br/>
              </xsl:for-each>
              <xsl:for-each select="fsv:OnAccountPaymentsText/fsv:Text">
                Beskrivelse<br/>
              </xsl:for-each>
              Periode<br/>
              Forbrug<br/>
              Beskrivelse<br/>
            </xsl:for-each>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <br/>
            <xsl:for-each select="fsv:DeliveryOnAccountPaymentSchedule">
              <xsl:for-each select="fsv:OnAccountPayments">
                <xsl:for-each select="fsv:DuePaymentDate">
                  <xsl:value-of select="."/>
                  <br/>
                </xsl:for-each>
                <xsl:value-of select="fsv:Amount"/>
                <br/>
              </xsl:for-each>
              <xsl:for-each select="fsv:OnAccountPaymentsText/fsv:Text">
                <xsl:value-of select="."/>
                <br/>
              </xsl:for-each>
              <xsl:value-of select="fsv:OnAccountMainInformation/fsv:InvoicedPeriod"/>
              <br/>
              <xsl:value-of select="fsv:OnAccountMainInformation/fsv:Quantity"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="fsv:OnAccountMainInformation/fsv:Quantity/@quantityUnitCode"/>
              <br/>
              <xsl:value-of select="fsv:OnAccountMainInformation/fsv:OnAccountMainText"/>
              <br/>
            </xsl:for-each>
          </font>
        </td>
      </tr>


      <!-- indsætter energiafgifter -->
      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <b>Energiafgifter</b>
            <br/>
            <xsl:for-each select="fsv:EnergyTaxInformation/fsv:EnergyTaxInformationDetail">
              Kode<br/>
              Beskrivelse<br/>
              Beløb<br/>
              Aconto<br/>
              Balance<br/>
            </xsl:for-each>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <br/>
            <xsl:for-each select="fsv:EnergyTaxInformation/fsv:EnergyTaxInformationDetail">
              <xsl:value-of select="fsv:DescriptionCode"/>
              <br/>
              <xsl:value-of select="fsv:Description"/>
              <br/>
              <xsl:value-of select="fsv:TaxEnergyAmount"/>
              <br/>
              <xsl:value-of select="fsv:TaxEnergyOnAccountAmount"/>
              <br/>
              <xsl:value-of select="fsv:TaxEnergyBalanceAmount"/>
              <br/>
            </xsl:for-each>
          </font>
        </td>
      </tr>


      <!-- indsætter gns. energipris -->
      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <b>Gennemsnitlige energipriser</b>
            <br/>
            <xsl:for-each select="fsv:ConsumptionAverage">
              Gennemsnitspris<br/>
              Beskrivelse<br/>
            </xsl:for-each>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <br/>
            <xsl:for-each select="fsv:ConsumptionAverage">
              <xsl:value-of select="fsv:AverageAmount"/>
              <br/>
              <xsl:value-of select="fsv:Text"/>
              <br/>
            </xsl:for-each>
          </font>
        </td>
      </tr>


      <!-- indsætter totalbeløb -->
      <tr></tr>
      <tr></tr>
      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font  face="Arial" size="2">
            <b>Fakturatotaler</b>
            <br/>
          </font>
        </td>
        <td  width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <br/>

          </font>
        </td>
      </tr>

      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">Sum af leverandørtotaler</font>
        </td>
        <td width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <!-- Totalpris eksklusiv moms -->
            <xsl:variable name="tot1" select="fsv:UtilityLegalTotals/fsv:UtilitySubTotalAmount"/>
            <xsl:value-of select="format-number($tot1, '##0.00')"/>
          </font>
        </td>
      </tr>
      <xsl:for-each select="fsv:UtilityLegalTotals/fsv:ServiceCharge">

        <xsl:variable name="ainfo">
          <xsl:choose>
            <xsl:when test="fsv:ServiceReasonCode != ''">
              <xsl:value-of select="fsv:ServiceReasonCode"/>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="binfo">
          <xsl:choose>
            <xsl:when test="fsv:ServiceIndicator = 'false'">ej momspligtigt</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="cinfo">
          <xsl:choose>
            <xsl:when test="$ainfo != '' and $binfo != ''">
              (<xsl:value-of select="$ainfo"/>, <xsl:value-of select="$binfo"/>)
            </xsl:when>
            <xsl:when test="$ainfo != '' and $binfo = ''">
              (<xsl:value-of select="$ainfo"/>)
            </xsl:when>
            <xsl:when test="$ainfo = '' and $binfo != ''">
              (<xsl:value-of select="$binfo"/>)
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <tr>
          <td width="30%" valign="top" bgcolor="#FFFFFF">
            <font face="Arial" size="2">
              <xsl:value-of select="fsv:ServiceID"/>
              <xsl:value-of select="$cinfo"/>
            </font>
          </td>
          <td width="70%" valign="top" bgcolor="#FFFFFF">
            <font face="Arial" size="2">
              <xsl:variable name="tot2" select="fsv:ServiceAmount"/>
              <xsl:value-of select="format-number($tot2, '##0.00')"/>
            </font>
          </td>
        </tr>
      </xsl:for-each>

      <!-- Foerste gennemloeb -->
      <xsl:for-each select="fsv:UtilityTaxTotal">
        <xsl:variable name="nuller">
          <xsl:choose>
            <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = ''">nul</xsl:when>
            <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0'">nul</xsl:when>
            <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.0'">nul</xsl:when>
            <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.00'">nul</xsl:when>
            <xsl:when test="fsv:TaxAmounts/fsv:TaxableAmount = '0.000'">nul</xsl:when>
            <xsl:otherwise>ejnul</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:choose>
          <!-- Momsfri andel -->
          <xsl:when test = "fsv:TaxTypeCode = 'ZERO-RATED' and $nuller = 'ejnul'">
            <tr>
              <td width="30%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">Momsfri andel</font>
              </td>
              <td width="70%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- Momsfri andel -->
                  <xsl:variable name="tot3" select="fsv:TaxAmounts/fsv:TaxableAmount"/>
                  <xsl:value-of select="format-number($tot3, '##0.00')"/>
                </font>
              </td>
            </tr>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>

      <!-- Andet gennemloeb -->
      <xsl:for-each select="fsv:UtilityTaxTotal">
        <xsl:choose>
          <!-- Momspligtig andel -->
          <xsl:when test = "fsv:TaxTypeCode = 'VAT'">
            <tr>
              <td width="30%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">Momsgrundlag</font>
              </td>
              <td width="70%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- Momsgrundlag -->
                  <xsl:variable name="tot6" select="fsv:TaxAmounts/fsv:TaxableAmount"/>
                  <xsl:value-of select="format-number($tot6, '##0.00')"/>
                </font>
              </td>
            </tr>
            <tr>
              <xsl:variable name="momspct" select="fsv:CategoryTotal/fsv:RatePercentNumeric"/>
              <td width="30%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  Total momsbeløb (<xsl:value-of select="format-number($momspct, '##0.00')"/>%)
                </font>
              </td>
              <td width="70%" valign="top" bgcolor="#FFFFFF">
                <font face="Arial" size="2">
                  <!-- Totalmoms -->
                  <xsl:variable name="tot4" select="fsv:TaxAmounts/fsv:TaxAmount"/>
                  <xsl:value-of select="format-number($tot4, '##0.00')"/>
                </font>
              </td>
            </tr>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>

      <tr>
        <td width="30%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">Fakturatotal incl moms</font>
        </td>
        <td width="70%" valign="top" bgcolor="#FFFFFF">
          <font face="Arial" size="2">
            <!-- Total inklusiv moms -->
            <xsl:variable name="tot5" select="fsv:UtilityLegalTotals/fsv:ToBePaidTotalAmount"/>
            <xsl:value-of select="format-number($tot5, '##0.00')"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="fsv:UtilityLegalTotals/fsv:Description"/>
          </font>
        </td>
      </tr>

      <tr>
        <td width="100%" valign="top" colspan="4" bgcolor="#FFFFFF" height="1">
          <hr color="#77B321" size="2" NOSHADE="true"/>
        </td>
      </tr>
    </table>
    <br/>
    <!-- Slut på Utility invoice (forbrugssted) -->

  </xsl:template>


</xsl:stylesheet>
